import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css',
})
export class AppRoot {
  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="menu-list" />
          <ion-route url="/drink" component="menu-item" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
